// Custom icon static resources
import { IconDefinition } from '@ant-design/icons-angular';

import {
	/** A **/
	mdiAccount,
	//mdiAccountCardDetails,
	mdiAccountEdit,
	mdiAccountGroup,
	mdiAccountPlus,
	mdiAccountRemove,
	mdiAccountSearch,
	mdiArrowExpandLeft,
	mdiArrowExpandRight,
	mdiAt,
	/** C **/
	mdiCameraPlus,
	mdiCodeBraces,
	mdiContentSave,
	/** D **/
	mdiDelete,
	mdiDiscord,
	/** H **/
	mdiHandshake,
	mdiHelp,
	mdiHome,
	/** I **/
	mdiIdentifier,
	/** L **/
	mdiLock,
	mdiLogout,
	/** N **/
	mdiNumeric,
	/** P **/
	mdiPencil,
	mdiPlus,
	mdiPound,
	mdiPower,
	/** R **/
	mdiRefresh,
	/** S **/
	//mdiSettings,
	/** T **/
	//mdiTextboxPassword,
	mdiTools,
	mdiTwitch,
	/** Y **/
	mdiYoutube
} from '@mdi/js';

const svg = '<svg viewBox="0 0 24 24">\n ';

/** A **/
const mdiAccountIcon: IconDefinition = {
	name: 'mdi:account',
	icon: svg + '<path d="' + mdiAccount + '" /> \n</svg>',
};
/*const mdiAccountCardDetailsIcon: IconDefinition = {
	name: 'mdi:account-card-details',
	icon: svg + '<path d="' + mdiAccountCardDetails + '" /> \n</svg>',
};*/
const mdiAccountEditIcon: IconDefinition = {
	name: 'mdi:account-edit',
	icon: svg + '<path d="' + mdiAccountEdit + '" /> \n</svg>',
};
const mdiAccountGroupIcon: IconDefinition = {
	name: 'mdi:account-group',
	icon: svg + '<path d="' + mdiAccountGroup + '" /> \n</svg>',
};
const mdiAccountPlusIcon: IconDefinition = {
	name: 'mdi:account-plus',
	icon: svg + '<path d="' + mdiAccountPlus + '" /> \n</svg>',
};
const mdiAccountRemoveIcon: IconDefinition = {
	name: 'mdi:account-remove',
	icon: svg + '<path d="' + mdiAccountRemove + '" /> \n</svg>',
};
const mdiAccountSearchIcon: IconDefinition = {
	name: 'mdi:account-search',
	icon: svg + '<path d="' + mdiAccountSearch + '" /> \n</svg>',
};
const mdiArrowExpandLeftIcon: IconDefinition = {
	name: 'mdi:arrow-expand-left',
	icon: svg + '<path d="' + mdiArrowExpandLeft + '" /> \n</svg>',
};
const mdiArrowExpandRightIcon: IconDefinition = {
	name: 'mdi:arrow-expand-right',
	icon: svg + '<path d="' + mdiArrowExpandRight + '" /> \n</svg>',
};
const mdiAtIcon: IconDefinition = {
	name: 'mdi:at',
	icon: svg + '<path d="' + mdiAt + '" /> \n</svg>',
};
/** C **/
const mdiCameraPlusIcon: IconDefinition = {
	name: 'mdi:camera-plus',
	icon: svg + '<path d="' + mdiCameraPlus + '" /> \n</svg>',
};
const mdiCodeBracesIcon: IconDefinition = {
	name: 'mdi:code-braces',
	icon: svg + '<path d="' + mdiCodeBraces + '" /> \n</svg>',
};
const mdiContentSaveIcon: IconDefinition = {
	name: 'mdi:content-save',
	icon: svg + '<path d="' + mdiContentSave + '" /> \n</svg>',
};
/** D **/
const mdiDeleteIcon: IconDefinition = {
	name: 'mdi:delete',
	icon: svg + '<path d="' + mdiDelete + '" /> \n</svg>',
};
const mdiDiscordIcon: IconDefinition = {
	name: 'mdi:discord',
	icon: svg + '<path d="' + mdiDiscord + '" /> \n</svg>',
};
/** H **/
const mdiHandshakeIcon: IconDefinition = {
	name: 'mdi:handshake',
	icon: svg + '<path d="' + mdiHandshake + '" /> \n</svg>',
};
const mdiHelpIcon: IconDefinition = {
	name: 'mdi:help',
	icon: svg + '<path d="' + mdiHelp + '" /> \n</svg>',
};
const mdiHomeIcon: IconDefinition = {
	name: 'mdi:home',
	icon: svg + '<path d="' + mdiHome + '" /> \n</svg>',
};
/** I **/
const mdiIdentifierIcon: IconDefinition = {
	name: 'mdi:identifier',
	icon: svg + '<path d="' + mdiIdentifier + '" /> \n</svg>',
};
/** L **/
const mdiLockIcon: IconDefinition = {
	name: 'mdi:lock',
	icon: svg + '<path d="' + mdiLock + '" /> \n</svg>',
};
const mdiLogoutIcon: IconDefinition = {
	name: 'mdi:logout',
	icon: svg + '<path d="' + mdiLogout + '" /> \n</svg>',
};
/** N **/
const mdiNumericIcon: IconDefinition = {
	name: 'mdi:numeric',
	icon: svg + '<path d="' + mdiNumeric + '" /> \n</svg>',
};
/** P **/
const mdiPencilIcon: IconDefinition = {
	name: 'mdi:pencil',
	icon: svg + '<path d="' + mdiPencil + '" /> \n</svg>',
};
const mdiPlusIcon: IconDefinition = {
	name: 'mdi:plus',
	icon: svg + '<path d="' + mdiPlus + '" /> \n</svg>',
};
const mdiPoundIcon: IconDefinition = {
	name: 'mdi:pound',
	icon: svg + '<path d="' + mdiPound + '" /> \n</svg>',
};
const mdiPowerIcon: IconDefinition = {
	name: 'mdi:power',
	icon: svg + '<path d="' + mdiPower + '" /> \n</svg>',
};
/** R **/
const mdiRefreshIcon: IconDefinition = {
	name: 'mdi:refresh',
	icon: svg + '<path d="' + mdiRefresh + '" /> \n</svg>',
};
/** S **/
/*const mdiSettingsIcon: IconDefinition = {
	name: 'mdi:settings',
	icon: svg + '<path d="' + mdiSettings + '" /> \n</svg>',
};*/
/** T **/
/*const mdiTextboxPasswordIcon: IconDefinition = {
	name: 'mdi:text-password',
	icon: svg + '<path d="' + mdiTextboxPassword + '" /> \n</svg>',
};*/
const mdiToolsIcon: IconDefinition = {
	name: 'mdi:tools',
	icon: svg + '<path d="' + mdiTools + '" /> \n</svg>',
};
const mdiTwitchIcon: IconDefinition = {
	name: 'mdi:twitch',
	icon: svg + '<path d="' + mdiTwitch + '" /> \n</svg>',
};
/** Y **/
const mdiYoutubeIcon: IconDefinition = {
	name: 'mdi:youtube',
	icon: svg + '<path d="' + mdiYoutube + '" /> \n</svg>',
};

export const ICONS = [
	/** A **/
	mdiAccountIcon,
	//mdiAccountCardDetailsIcon,
	mdiAccountEditIcon,
	mdiAccountGroupIcon,
	mdiAccountPlusIcon,
	mdiAccountRemoveIcon,
	mdiAccountSearchIcon,
	mdiArrowExpandLeftIcon,
	mdiArrowExpandRightIcon,
	mdiAtIcon,
	/** C **/
	mdiCameraPlusIcon,
	mdiCodeBracesIcon,
	mdiContentSaveIcon,
	/** D **/
	mdiDeleteIcon,
	mdiDiscordIcon,
	/** H **/
	mdiHandshakeIcon,
	mdiHelpIcon,
	mdiHomeIcon,
	/** I **/
	mdiIdentifierIcon,
	/** L **/
	mdiLockIcon,
	mdiLogoutIcon,
	/** N **/
	mdiNumericIcon,
	/** P **/
	mdiPencilIcon,
	mdiPlusIcon,
	mdiPoundIcon,
	mdiPowerIcon,
	/** R **/
	mdiRefreshIcon,
	/** S **/
	//mdiSettingsIcon,
	/** T **/
	//mdiTextboxPasswordIcon,
	mdiToolsIcon,
	mdiTwitchIcon,
	/** Y **/
	mdiYoutubeIcon
];
