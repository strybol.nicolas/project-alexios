
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'shortenDiscord'
})
export class ShortenDiscordPipe implements PipeTransform {

	transform(value: any, limit: number) {

		let test: string[] = value.split('#');
		let result;

		if( test[0].length > limit ) {
			result = test[0].substring(0, (limit - 4)) + '...#' + test[1];

			return result;
		}
		
		return value;
	}
}