
export class User {

	/** Constructor **/
	constructor(
		public id: string,
		public discordInfo?: DiscordInfo
	) { }


	/** Setters **/

	/**
	 * Replaces existing id with the provided new one
	 * @param id 
	 */
	setId(id: string) {
		this.id = id;
	}

	/**
	 * Replaces existing discordInfo with the provided new one
	 * @param discordInfo 
	 */
	setDiscordInfo(discordInfo: DiscordInfo) {
		this.discordInfo = discordInfo;
	}

}


export class DiscordInfo {

	/** Constructor **/
	constructor(
		public id: string, 
		public username: string,
		public email: string,
		public roles?: string[]
	) { }

}