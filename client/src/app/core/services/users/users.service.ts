
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

import { User, DiscordInfo } from '../../models/user.model';



@Injectable({
	providedIn: 'root'
})
export class UsersService {

	users: User[] = [];

	url: string = 'http://localhost:3000';
	/** Constructor **/
	constructor(
		private http: HttpClient
	) { }


	/**
	 * Fetches all users
	 */
	getUsers(): Observable<User[]> {

		//return of(this.populate()).pipe(delay(5000));
		return this.http.get<User[]>(this.url + '/users');
	}

	/**
	 * Fetches a single User witht he provided id
	 * @param id 
	 */
	getUser(id: string): Observable<User> {
		
		return this.http.get<User>(this.url + '/users/' + id).pipe(delay(1000));
	}

	/**
	 * Adds a new User to the DB
	 * @param user 
	 */
	createUser(user: User) {
		console.log("== createUser(user) ==");
		this.http.post(this.url + '/users/', user)
			.subscribe(
				() => {
					console.log("Create worked!");
				},
				err => {
					console.log("Error while creating!");
				}
			);
	}

	/**
	 * Updates the existing User, with the provided id, in the DB
	 * @param id 
	 * @param user 
	 */
	upateUser(id: string, user: User) {
		console.log("== upateUser(user) ==");
		this.http.put(this.url + '/users/' + id, user)
			.subscribe(
				() => {
					console.log("Update worked!");
				},
				err => {
					console.log("Error while updating!");
				}
			);
	}

	/**
	 * Deletes the User, with the provided id, from the DB 
	 * @param id 
	 */
	deleteUser(id: string) {
		this.http.delete(this.url + '/users/' + id)
			.subscribe(
				() => {
					console.log("Delete worked!");
				},
				err => {
					console.log("Error while deleting!");
				}
			);
	}


	getEmptyUser(): User {
		return new User(
			"-9999",
			new DiscordInfo("-9999", "", "", [] )
		);
	}

}
