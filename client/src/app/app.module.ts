import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const MODULES = [
	HttpClientModule,
	BrowserModule,
	BrowserAnimationsModule,
	FormsModule,
	ReactiveFormsModule
];

import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';

registerLocaleData(en);

import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';

// ng-zorro-antd modules
import { NZ_I18N, en_GB } from 'ng-zorro-antd/i18n';

// Icons
import { IconsProviderModule } from './icons-provider.module';


// Shared Modules
import { SharedModule } from './shared/shared.module';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Shared Components
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';

const SHAREDCOMPONENTS = [
	HeaderComponent,
	SidebarComponent
];

// Page modules
import { UsersModule } from './pages/users/users.module';

// Page Components
import { HomeComponent } from './pages/home/home.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const COMPONENTS = [
	HomeComponent,
	PageNotFoundComponent
];


@NgModule({
	declarations: [
		AppComponent,
		...SHAREDCOMPONENTS,
		...COMPONENTS	
	],
	imports: [
		...MODULES,
		SharedModule,
		IconsProviderModule,
		AppRoutingModule,
		UsersModule
		//DragDropModule,
		//ScrollingModule
	],
	providers: [{ 
		provide: NZ_I18N, useValue: en_GB 
	}],
	bootstrap: [AppComponent]
})
export class AppModule { }
