import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	isCollapsed: boolean;


	/** Constructor & Lifecycle hooks **/
	constructor() { }

	ngOnInit() {
		this.isCollapsed = true;	// Set sidebar initial state to collapsed
	}


	/**
	 * Callback for isCollapedChanged EventEmitter, executes when 
	 * the user wishes to expand/collapse sidebar
	 */
	onIsCollapseChange() {
		console.log("== onIsCollapseChange ==");
		this.isCollapsed = !this.isCollapsed;
	}

}
