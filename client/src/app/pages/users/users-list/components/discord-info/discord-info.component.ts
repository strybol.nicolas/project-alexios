import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DiscordInfo } from '../../../../../core/models/user.model';

@Component({
	selector: 'discord-info-fields',
	templateUrl: './discord-info.component.html',
	styleUrls: ['./discord-info.component.scss']
})
export class DiscordInfoComponent implements OnInit {

	@Output() valuesChanged = new EventEmitter<FormGroup>();

	@Input() 
	set setDiscordInfo(data) {
		console.log("== setDiscordInfo() ==");
		console.log(data);
		
		if( data ) {
			this.initForm(data);
		} else {
			this.initComplete = true;
		}
	}

	form: FormGroup;

	listOfRoles: string[] = [
		"Admin", "Mod",
		"Streamer", "Youtuber",
		"Member"
	];

	initComplete: boolean = false;


	/** Constructor & Lifecycle hooks **/
	constructor() { }

	ngOnInit(): void { 
		console.log("== ngOnInit() ==");

		this.form = new FormGroup({
			id: new FormControl(null, [ Validators.required ]),
			username: new FormControl(null, [ Validators.required ]),
			email: new FormControl(null, [ Validators.required, Validators.email ]),
			roles: new FormControl(null)
		});

		this.form.valueChanges
			.subscribe(
				values => {
					if( this.initComplete ) {
						this.valuesChanged.emit(this.form);
					}
				}
			);
	}


	/**
	 * Assigns the received DiscordInfo properties into the correct FormControls
	 * @param discordInfo 
	 */
	initForm(discordInfo: DiscordInfo) {
		console.log("== initForm ==");
		console.log(discordInfo);	
		
		for( let prop in discordInfo ) {
			
			this.form
				.get(prop)
				.setValue(discordInfo[prop]);
		}
		
		this.initComplete = true;
	}

}
