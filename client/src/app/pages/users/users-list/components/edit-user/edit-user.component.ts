
import { 
	Component, OnInit, OnDestroy,
	Input, Output, EventEmitter 
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

/** Custom Services & Models **/
import { User, DiscordInfo } from 'src/app/core/models/user.model';
import { UsersService } from 'src/app/core/services/users/users.service';


@Component({
	selector: 'edit-user',
	templateUrl: './edit-user.component.html',
	styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit, OnDestroy {

	@Output() onModalClosed = new EventEmitter<User | void>();

	@Input() 
	set setData(receivedData) {
		console.log("== setData() ==");
		this.data = receivedData;		
	}

	subscription: Subscription;

	data: { isNew: boolean, userId: string };
	user: User = new User("-9999");

	temp = {
		discordInfo: {
			valid: false,
			data: {}
		}
	};
	
	isVisible: boolean = false;
	isLoading: boolean = true;


	/** Constructor & Lifecycle hooks **/
	constructor(private usersService: UsersService) { }

	ngOnInit(): void { 
		console.log("== ngOnInit() ==");

		if( this.data.isNew ) {
			console.log("New User!");
			this.user = new User(this.data.userId);

			this.isVisible = true;
			this.isLoading = false;
		} else if( this.data.userId !== "" ) {
			console.log("Edit User!");
			this.subscription = this.usersService.getUser(this.data.userId)
				.subscribe(
					(user: User) => {
						this.user = user;

						this.isLoading = false;
					}
			);
				
			this.isVisible = true;
		}
	}

	ngOnDestroy() {
		console.log("== 'edit-user.component' destroyed ==");
		if( this.subscription ) {
			console.log("Unsubscribing!");
			this.subscription.unsubscribe();
		}
	}

	/** nz-modal methods **/

	/**
	 * On click event to close modal, the code is the same for cancel and save.
	 * @param isSave 
	 */
	closeModal(isSave: boolean) { 
		this.modalClosing(isSave);
	}

	/**
	 * Handler for closeModal()
	 * @param save 
	 */
	modalClosing(save: boolean) {
		this.isVisible = false;

		for( let prop in this.temp ) {
			this.user[prop] = this.temp[prop].data;
		}
		console.log(this.user);
		this.onModalClosed.emit( save ? this.user : null);

		this.isLoading = true;
	}


	/** EventEmitter Callback **/

	/**
	 * 
	 */
	fetchDiscordInfo(form: FormGroup) {
		console.log("== fetchDiscordInfo ==");
		console.log("form = " + form);
		this.temp.discordInfo.valid = form.valid;
		
		this.temp.discordInfo.data =
			new DiscordInfo(
				form.value.id, 
				form.value.username, 
				form.value.email, 
				form.value.roles
			);
			
	}

	/**
	 * Verifies if different forms are valid, to enable the save button
	 */
	allowSave(): boolean {

		for( let prop in this.temp ) {
			if( !this.temp[prop].valid ) {
				return false;
			}
		}

		return true;
	}
}
