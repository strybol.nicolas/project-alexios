import { Component, OnInit } from '@angular/core';

import { v4 as uuidv4 } from 'uuid';

/** Models & Services **/
import { User } from '../../../core/models/user.model';

import { UsersService } from '../../../core/services/users/users.service';


@Component({
	selector: 'app-users-list',
	templateUrl: './users-list.component.html',
	styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

	usersList: User[] = [];

	modalData: { isNew: boolean, userId: string } = {
		isNew: false,
		userId: ""
	}
	
	isLoading: boolean = true;


	/** Constructor & Lifecycle hooks **/
	constructor(private usersService: UsersService) { }

	ngOnInit(): void {

		this.usersService.getUsers()
			.subscribe(
				(users: User[]) => {
					this.usersList = users;

					this.isLoading = false;
				}
			);
		
		console.log(this.usersList);
	}


	/** **/

	/**
	 * 
	 */
	new() {
		console.log("New User!");

		this.modalData = {
			isNew: true,
			userId: uuidv4()
		};
	}

	/**
	 * Sends
	 * @param id 
	 */
	edit(id: string) {
		console.log("Edit User!");
		this.modalData = {
			isNew: false,
			userId: id
		};
	}

	/**
	 * Sends request to delete user with specified id
	 * @param id
	 */
	delete(id: string) {
		this.usersService.deleteUser(id);
	}

	refreshList() {

		this.isLoading = true;

		this.usersService.getUsers()
			.subscribe(
				(users: User[]) => {
					this.usersList = users;

					this.isLoading = false;
				}
			);
	}


	/** nz-popconfirm methods **/

	/**
	 * Asks for user confirmation before deleting the desired user
	 * @param id 
	 */
	onDeleteConfirm(id: string) {
		console.log("Confirmed id='" + id + "' for Deletion!")
		this.delete(id);
	}

	/**
	 * Closes the nz-popconfirm pop-up
	 */
	onDeleteCancel() { }


	/** EventEmitter Callback **/

	/**
	 * Handler for when the modal is closed
	 * 
	 * IF user is null, we just reset modalData
	 * IF user exists, we check if it's new or edit and act accordingly 
	 * 
	 * @param event 
	 */
	ModalClosedHandler(user: User) {
		
		if( user ) {

			if( this.modalData.isNew ) {
				console.log("Create New User!");
				this.usersService.createUser(user);				
			} else {
				console.log("Update Existing User!");
				this.usersService.upateUser(user.id, user);
			}
		}
		
		this.modalData = {
			isNew: false,
			userId: ""
		};
	}
}
