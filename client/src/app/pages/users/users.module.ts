
import { NgModule } from '@angular/core';

import { NgZorroAntdModule } from '../../shared/ng-zorro-antd.module';

/** Shared Modules **/
import { SharedModule } from '../../shared/shared.module';

/** Components **/
import { UsersListComponent } from './users-list/users-list.component';
import { EditUserComponent } from './users-list/components/edit-user/edit-user.component';

import { UsersStatisticsComponent } from './users-statistics/users-statistics.component';
import { DiscordInfoComponent } from './users-list/components/discord-info/discord-info.component';


const COMPONENTS = [
	UsersListComponent,
	EditUserComponent,
	DiscordInfoComponent,

	UsersStatisticsComponent,
];


@NgModule({
	declarations: [
		...COMPONENTS,
		
	],
	imports: [
		SharedModule,
		NgZorroAntdModule
	],
	exports: [
		...COMPONENTS
	],
	providers: [],
})
export class UsersModule { }