import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';

import { UsersListComponent } from './pages/users/users-list/users-list.component';
import { UsersStatisticsComponent } from './pages/users/users-statistics/users-statistics.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [{
	path: '', 
	pathMatch: 'full', 
	redirectTo: '/home' 
}, { 
	path: 'home', 
	component: HomeComponent
}, {
	path: 'users',
	children: [{ 
		path: 'list', component: UsersListComponent
	}, { 
		path: 'statistics', component: UsersStatisticsComponent
	}]   
	//loadChildren: () => import('./pages/users/users.module').then(m => m.UsersModule)
}, {
	path: '**', 
	component: PageNotFoundComponent 
}];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }
