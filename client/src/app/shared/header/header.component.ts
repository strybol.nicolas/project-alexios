
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	@Output() isCollapedChanged = new EventEmitter<void>();

	@Input() isCollapsed: boolean;

	title: string;


	/** Constructor & Lifecycle hooks **/
	constructor() { }

	ngOnInit(): void {
		this.title = "== Project Alexios ==";
	}

	
	/**
	 * When trigger gets clicked, emits a void event to parent.  
	 */
	onTriggerClicked() {
		this.isCollapedChanged.emit();
	}
}
