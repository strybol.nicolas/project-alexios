
import { NgModule } from '@angular/core';

/** Shared Modules **/
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';

import { NgZorroAntdModule } from './ng-zorro-antd.module';

const MODULES = [
	CommonModule,
	FormsModule,
	ReactiveFormsModule,
	FlexLayoutModule,
	NgZorroAntdModule
];

/** Pipes **/
import { ShortenDiscordPipe } from '../core/pipes/shorten-discord.pipe';

const PIPES = [
	ShortenDiscordPipe
];

@NgModule({
	declarations: [
		...PIPES
	],
	imports: [
		...MODULES
	],
	exports: [
		...MODULES,
		...PIPES
	],
	providers: [],
})
export class SharedModule { }

